#!/bin/env python3

"""
converts HTML presentations to PDFs using DECKTAPE and DOCKER
converts of all files in a folder

usage:
   options [html SOURCE FOLDER or HTMLFILE] [pdf TARGET FOLDER]

requires docker installed

(c) O. Lindemann
"""

from subprocess import call
from os import path, listdir, rename, makedirs
import sys
import uuid

__version__ = 0.3

def decktape(html_file, target_folder):
    # using docker image

    target_folder = path.abspath(target_folder)
    try:
        makedirs(target_folder)
    except:
        pass

    fld, flname = path.split(path.abspath(html_file))
    while True:
        tmp_pdf_file = "tmp-" + str(uuid.uuid4())[:6] + ".pdf"
        if not path.isfile(path.join(fld, tmp_pdf_file)):
            break

    cmd = f"docker run --rm -t -v {fld}:/slides -v {fld}:/home/user astefanutti/decktape /home/user/{flname} {tmp_pdf_file}"
    print(f"-- converting {flname}")
    print(f"- source folder: {fld}\n- target folder: {target_folder}")
    print("- " + cmd)

    call(cmd.split(" "))

    pdf_file, _ = path.splitext(flname)
    pdf_file = path.join(target_folder, pdf_file + ".pdf")
    rename(path.join(fld, tmp_pdf_file), pdf_file)


def decktape_all(source_path, target_folder):

    if path.isfile(source_path):
        if target_folder == source_path:
            target_folder, _ = path.split(target_folder) # just folder
        decktape(source_path, target_folder)

    else: # multiple files
        for x in listdir(source_path):
            x = path.join(source_path, x)
            if path.isfile(x) and x.endswith(".html"):
                decktape(x, target_folder)


if __name__ == "__main__":

    if len(sys.argv)>1:
        SOURCE_PATH = sys.argv[1]
    else:
        SOURCE_PATH = "."

    if len(sys.argv)>2:
        TARGET_PATH = sys.argv[2]
    else:
        TARGET_PATH = SOURCE_PATH

    decktape_all(SOURCE_PATH, TARGET_PATH)
