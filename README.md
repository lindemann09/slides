# Hosting HTML and PDF slides

* Copy (self contained) HTML presentaions in the folder `public` and commit.
* PDF will be created by Gitlab-CI.
* HTML and PDF slides can be found at https://lindemann09.gitlab.io/slides

(c) Oliver Lindemann, Erasmus University Rotterdam
